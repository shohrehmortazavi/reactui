﻿import { combineReducers } from 'redux';
import { ArticleReducer } from '../../Components/Articles/ArticleReducer';
import { GalleryReducer } from '../../Components/Gallery/GalleryReducer';

const RootReducer = ArticleReducer;// combineReducers({ ArticleReducer, GalleryReducer });

export default RootReducer;