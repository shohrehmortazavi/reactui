﻿//import { baseUrl } from '../Config';

const baseUrl = 'http://localhost:51890/api/website';

/*** Articles ***/
export const GetArticlesApiUrl = baseUrl + '/Articles/GetArticle';
export const InsertArticleApiUrl = baseUrl + '/Articles/InsertArticle';
export const UpdateArticleApiUrl = baseUrl + '/Articles/UpdateArticle';
export const DeleteArticleApiUrl = baseUrl + '/Articles/DeleteArticle';