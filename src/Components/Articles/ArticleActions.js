﻿import { GET, INSERT, UPDATE, DELETE } from '../../common/SharedStore/ActionTypes';

export function GetArticles(articles) {
    return {
        type: GET,
        payload: articles
    }
}

export function InsertArticle(article) {
    return {
        type: INSERT,
       payload: article
    }
}

export function UpdateArticle(article) {
    return {
        type: UPDATE,
        payload: article
    }
}

export function DeleteArticle(articleId) {
    return {
        type: DELETE,
        payload: articleId
    }
}