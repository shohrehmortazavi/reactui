﻿import { GET, INSERT, UPDATE, DELETE } from '../../Common/SharedStore/ActionTypes';

export const ArticleReducer =(state = [], action) =>{

    switch (action.type) {
        case GET:
            return {
                ...state,
                articles: state
            }
        case INSERT:
            return {
                ...state,
                articles: state.concat([action.payload])
            }
        case UPDATE:
            return {
                ...state,
                articles: state.map(article => article.ArticleId === action.payload.ArticleId ? action.payload : article)
            }
        case
            DELETE:
            return {
                ...state,
                articles: state.filter(article => article.ArticleId !== action.payload)
            }
        default:
            return state;
    }
}