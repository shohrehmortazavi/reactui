﻿import React, { Component } from 'react';
import './Article.css';

class Article extends Component {

    constructor(props) {
        super(props);
    }

    render = () => {
        let article = this.props.article;
        return (
            <div className="col-md-4">
                <div className="article-post">
                    <div className="article-thumb">
                        <img src={article.ArticleImages[0]} alt={article.Title} />
                    </div>

                    <div className="article-content">
                        <ul className="meta-info">
                            <li className="article-date"><a href="#">{article.PersianCreationDate}</a></li>
                        </ul>
                        <h4 className="article-title">
                            <a href={article.ArticleURL}>{article.Title}</a>
                        </h4>
                        <p className="content">
                            {article.Body}
                        </p>
                        <a href={article.ArticleURL} className="read-more">
                            ادامه مطلب <i className="fas fa-angle-double-left"></i>
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}

export default Article;