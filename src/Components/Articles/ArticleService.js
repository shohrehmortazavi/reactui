﻿import axios from 'axios';
import { GetArticlesApiUrl, InsertArticleApiUrl, UpdateArticleApiUrl, DeleteArticleApiUrl } from '../../Constants/WebServicePaths';

let query = JSON.parse(
    `{
    "PageIndex": null,
    "PageSize": null,
    "Filters": null,
    "Sorts": null,
    "Fields": ["ArticleId", "ArticleCategoryId", "ArticleCategoryTitle", "Title", "Body", "AllowComments", "Tags", "IsActive", "CreationDate", "PersianCreationDate", "ArticleURL", "ArticleImages"]
    }`);

export class ArticleService {
    GetArticles(callback) {
        this.SendRequest("post", GetArticlesApiUrl, callback, query);
    }

    InsertArticle(article, callback) {
        this.SendRequest("post", InsertArticleApiUrl, callback, article);
    }

    UpdateArticle(article, callback) {
        this.SendRequest("post", UpdateArticleApiUrl, callback, article);
    }
    DeleteArticle(articleId, callback) {
        this.SendRequest("delete", DeleteArticleApiUrl, callback, articleId);
    }

    async SendRequest(method, url, callback, data) {
        try {
            callback((await axios.request({
                method: method,
                url: url,
                data: data
            })).data);

        } catch (e) {

        }
    }
}