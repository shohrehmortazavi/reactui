﻿import { GET, INSERT, UPDATE, DELETE } from '../../Common/SharedStore/ActionTypes';
import { ArticleService } from './ArticleService';

export const ArticleMiddleware = () => {
    let articleService = new ArticleService();

    next => action => {
        switch (action.type) {
            case GET:
                if (ArticleService.getsate() === 0) {
                    articleService.GetArticles(data =>
                        next({ ...action, payload: data }));
                }
                break;
            case INSERT:
                articleService.InsertArticle(action.payload, data => next({ ...action, payload: data }));
                break;
            case UPDATE:
                articleService.UpdateArticle(action.payload, data => next({ ...action, payload: data }));
                break;
            case DELETE:
                articleService.DeleteArticle(action.payload, () => next(action));
                break;
            default:
                next(action);
        }

    }
}