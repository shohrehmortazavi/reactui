﻿import React, { Component } from 'react';
import Article from './Article';
import { ArticleService } from './ArticleService';
import { connect } from 'react-redux';

const mapStateToProps = state => {
    articles: state.articles
}

class ArticleContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            articles: []
        }
        this.articleService = new ArticleService();
    } 

    render = () => {
        return (
            <React.Fragment>
                <section id="blog-content" className="section-padding">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 col-12 text-center center">
                                <div className="section-title">
                                    <h2>مقالات</h2>
                                    <hr />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            {
                                this.state.articles.map(ar => { <Article article={ar} /> })
                            }
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }

    componentDidMount() {
        this.articleService.GetArticles(data => this.setState({ articles: data }));
    }
}


export default connect(mapStateToProps,null)(ArticleContainer);